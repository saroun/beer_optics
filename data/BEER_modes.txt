# Operation modes
# No	ID	description	comment	SL3_w	SL3_h	SL2_w	SL2_h	SL1_w	SL1_h	GEX1on	lambda	cmode	wmode	
0	F0	Maximum white beam	never used in experiment	100.0	100.0	0.0	0.0	0.0	0.0	1	2.10	0	0
1	F1	Accident full beam	F0 with FC choppers running	100.0	100.0	0.0	0.0	0.0	0.0	1	3.10	0	4
2	F2	Maximum reduced experimental beam	F1, with diffraction slit on	10.0	20.0	0.0	0.0	0.0	0.0	1	3.10	0	4
3	PS0	Maximum intensity,  pulse shaping	diffraction, max. slit	10.0	20.0	25.0	0.0	0.0	0.0	1	2.10	1	1
4	PS1	High flux, pulse shaping	in-situ thermo mechanical experiment	5.0	10.0	25.0	0.0	0.0	0.0	1	2.10	1	1
5	PS2	Medium resolution, pulse shaping	in-situ thermo mechanical experiment	5.0	10.0	20.0	0.0	35.0	0.0	1	2.10	2	1
6	PS3	High resolution, pulse shaping	in-situ thermo mechanical experiment	5.0	10.0	15.0	0.0	20.0	0.0	1	2.10	3	1
7	M0	Maximum intensity,  modulation	diffraction, max. slit	10.0	20.0	25.0	0.0	0.0	0.0	1	2.10	4	1
8	M1	High flux, modulation	strain scanning	2.0	5.0	25.0	0.0	0.0	0.0	1	2.10	4	1
9	M2	Medium resolution, modulation	strain scanning	1.0	3.0	18.0	0.0	35.0	0.0	1	2.10	5	1
10	M3	High resolution, modulation	strain scanning	1.0	3.0	8.0	50.0	12.0	0.0	0	2.10	6	1
11	IM0	White beam	imaging	0.0	0.0	25.0	25.0	10.0	10.0	0	3.00	0	1
12	IM1	High flux, pulse shaping	imaging, Bragg edge	0.0	0.0	25.0	25.0	10.0	10.0	0	3.50	1	1
13	SANS	White beam	SANS	10.0	10.0	15.0	15.0	20.0	20.0	0	6.00	0	1
14	DS0	diffraction + SANS, modulation	in-situ thermomechanical experiment	5.0	10.0	25.0	25.0	40.0	40.0	0	4.00	7	2
15	DS1	diffraction + SANS, pulse shaping	in-situ thermomechanical experiment	5.0	10.0	25.0	25.0	40.0	40.0	0	2.10	1	3
16	M4	High resolution, modulation x 4	strain scanning	1.0	3.0	8.0	50.0	12.0	0.0	0	2.10	8	1

# Resolution chopper modes [cmode], frequency in Hz
# ID	 	description	 	PSC1	PSC2	PSC3	MCA	MCB	MCC	
0		full pulse		0	0	0	0	0	0
1		PSC, high flux		168	0	-168	0	0	0
2		PSC, medium resolution		168	-168	0	0	0	0
3		PSC, high resolution		168	-168	0	0	0	0
4		MC, high flux		0	0	0	70	0	0
5		MC, medium resolution		0	0	0	140	0	0
6		MC, high resolution		0	0	0	280	0	0
7		MC, SANS mode		0	0	0	0	0	70
8		MC, AB mode		0	0	0	140	280	0

# Resolution chopper modes [cphases], angular shift in deg
# ID	 	description	 	PSC1	PSC2	PSC3	MCA	MCB	MCC	
0		full pulse		0	0	0	0	0	0
1		PSC, high flux		72	0	72	0	0	0
2		PSC, medium resolution		72	72	0	0	0	0
3		PSC, high resolution		72	72	0	0	0	0
4		MC, high flux		0	0	0	0	0	0
5		MC, medium resolution		0	0	0	0	0	0
6		MC, high resolution		0	0	0	0	0	0
7		MC, SANS mode		0	0	0	0	0	-90
8		MC, AB mode		0	0	0	0	0	0

# Wavelength selection chopper modes [wmode], frequency in Hz
# ID	 	description	 	FC1A	FC1B	FC2A	FC2B	
0		full beam		0	0	0	0
1		single frame		28	0	14	0
2		pulse suppression, double frame		7	0	7	0
3		alternating frames		14	63	0	7
4		full beam with FC2A		0	0	14	0

# Wavelength selection chopper modes [wphases], angular shift in deg
# ID	 	description	 	FC1A	FC1B	FC2A	FC2B	
0		full beam		0	0	0	0
1		single frame		6	0	0	0
2		pulse suppression, double frame		12	0	0	0
3		alternating frames		-9	0	0	0
4		full beam with FC2A		0	0	0	0
